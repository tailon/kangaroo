# Kangaroo 
Kangaroo is a SQL client and admin tool for popular databases(SQLite / MySQL / PostgreSQL / ...) on Windows / MacOS / Linux, support table design, query, model, sync, export/import etc, focus on comfortable, fun and developer friendly.

__Read this in other languages:__ English | [中文(Chinese)](./README.md)

## Official website / 官方网站
[English](https://www.datatable.online/?from=gitee) | [中文(Chinese)](https://www.datatable.online/zh/?from=gitee)

## Support database
Database support capability level: __Planned__ / __Partial__ / __Full(:100:)__

| Database    | Version | Query     | Editing   | Designer  | Export    | Import    | Hint      | Modeling | DB Sync |
|-------------|---------|-----------|-----------|-----------|-----------|-----------|-----------|----------|---------|
| SQLite      | 3.0 +   | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | in progress | Planned |
| MySQL       | 5.5 +   | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | in progress | Planned |
| MariaDB     | 10.0 +  | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | in progress | Planned |
| PostgreSQL  | 9.0 +   | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | Full:100: | in progress | Planned |
| Redis       |         | Planned   | Planned   | Planned   | Planned   | Planned   | Planned   | Planned  | Planned |
| Oracle      |         |           |           |           |           |           |           |          |         |
| SQL Server  |         |           |           |           |           |           |           |          |         |

**Hint**: Code intellisense or Code autocomplete


## Release
Development version keep one version(vNext) only from year 2023, App version and its packages will be updated regularly(one or two weeks), Beta and Stable version depend on test result and stabilization.

| Architect | Windows         | MacOS           | Linux           | iOS             | Android         | Harmony         |
|-----------|-----------------|-----------------|-----------------|-----------------|-----------------|-----------------|
| x86-64 | [v2.1.1](https://www.datatable.online/en/download/v2.1.1.221201?from=github&os=windows) | [v2.1.1](https://www.datatable.online/en/download/v2.1.1.221201?from=github&os=macos) | [v2.1.1](https://www.datatable.online/en/download/v2.1.1.221201?from=github&os=linux) |
| ARM64 | | | | | | |
| RISCV64 | | | | | | |

## Support the Project
If you like Kangaroo and you want to support its development, you could __scan QR code__ to donate via PayPal / Wechat / Alipay.

![Support project](./images/pay_wide.png)

## Screenshots
![Start page of connection](./images/kangaroo-start.png)
![Object explorer](./images/kangaroo-explorer.png)
![Object search](./images/kangaroo-search.png)
![Kangaroo grid view in table with custom columns](./images/kangaroo-grid.png)
![Kangaroo grid view in table with where statement](./images/kangaroo-grid2.png)
![Kangaroo grid view in form](./images/kangaroo-form.png)
![Kangaroo query view](./images/kangaroo-query.png)
![Kangaroo schema designer form](./images/kangaroo-designer.png)
![Kangaroo schema designer form with preview](./images/kangaroo-designer2.png)
![Kangaroo view designer](./images/kangaroo-view.png)
![Kangaroo function designer](./images/kangaroo-function.png)
![Kangaroo visual builder](./images/kangaroo-sql-builder.png)
![Kangaroo export assistant](./images/kangaroo-export.png)
![Kangaroo import assistant](./images/kangaroo-import.png)
![Kangaroo setting dialog with dark theme](./images/kangaroo-setting.png)
![Kangaroo shortcut setting dialog](./images/kangaroo-shortcut.png)
