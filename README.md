# 袋鼠数据库工具 / Kangaroo
袋鼠 是一款为热门数据库系统打造的管理客户端(SQLite / MySQL / PostgreSQL / ...) ，支持建表、查询、模型、同步、导入导出等功能，支持 Windows / Mac / Linux 等操作系统，力求打造成好用、好玩、开发友好的SQL工具。

__其他语言版本:__ 中文 | [英文(English)](./README_en.md)

## 官方网站 / Official website
[中文](https://www.datatable.online/zh/?from=gitee) | [英文(English)](https://www.datatable.online/?from=gitee)


## 支持的数据库系统
数据库支持能力级别: __已计划__ / __部分__ / __支持(:100:)__

| 数据库       | 支持版本 | SQL 查询     | 数据编辑   | 表设计器  | 导出    | 导入    | 智能提示      | 模型化 | 数据同步 |
|-------------|---------|--------------|------------|----------|---------|--------|---------------|-------|---------|
| SQLite      | 3.0 +   | 支持:100: | 支持:100: | 支持:100: | 支持:100: | 支持:100: | 支持:100: | **进行中**  | 已计划 |
| MySQL       | 5.5 +   | 支持:100: | 支持:100: | 支持:100: | 支持:100: | 支持:100: | 支持:100: | **进行中**  | 已计划 |
| MariaDB     | 10.0 +  | 支持:100: | 支持:100: | 支持:100: | 支持:100: | 支持:100: | 支持:100: | **进行中**  | 已计划 |
| PostgreSQL  | 9.0 +   | 支持:100: | 支持:100: | 支持:100: | 支持:100: | 支持:100: | 支持:100: | **进行中**  | 已计划 |
| Redis       |         | 已计划   | 已计划   | 已计划   | 已计划   | 已计划   | 已计划   | 已计划  | 已计划 |
| Oracle      |         |           |           |           |           |           |           |          |         |
| SQL Server  |         |           |           |           |           |           |           |          |         |

## 版本发布
从 2023年开始，开发版统一使用一个版号(vNext)，App 版本和发布包定期更新，计划一周或两周发布一个, Beta 版本和稳定版本根据版本质量评估情况不定期发布。

| CPU 架构  | Windows         | MacOS           | Linux           | iOS             | Android         | Harmony         |
|-----------|-----------------|-----------------|-----------------|-----------------|-----------------|-----------------|
| x86-64 | [v2.1.1](https://www.datatable.online/en/download/v2.1.1.221201?from=github&os=windows) | [v2.1.1](https://www.datatable.online/en/download/v2.1.1.221201?from=github&os=macos) | [v2.1.1](https://www.datatable.online/en/download/v2.1.1.221201?from=github&os=linux) |
| ARM64 | | | | | | |
| RISCV64 | | | | | | |

## 支持和赞助项目
如果您觉得袋鼠数据库工具有用且愿意支持它持续丰富功能，您可以通过如下方式支持和赞助项目，扫码即可通过 PayPal / 微信支持 / 支付宝完成付款。<br/>
![支持项目](./images/pay_wide.png)

## 微信公众号
为即时推送关于袋鼠的大篇幅文章和视频，故开通了微信公众号： 袋鼠数据库工具 (DataTableOnline)，有兴趣的同学可关注。 <br/>
![袋鼠微信公众号](./images/kangaroo_mp.png)

## 工具界面快照
![链接首页](./images/kangaroo-start.png)
![对象浏览器](./images/kangaroo-explorer.png)
![对象搜索](./images/kangaroo-search.png)
![数据网格（自定义列）](./images/kangaroo-grid.png)
![数据网格（条件查询）](./images/kangaroo-grid2.png)
![数据编辑表单](./images/kangaroo-form.png)
![查询界面](./images/kangaroo-query.png)
![表设计器](./images/kangaroo-designer.png)
![视图设计器](./images/kangaroo-view.png)
![函数设计器](./images/kangaroo-function.png)
![可视化SQL构建器](./images/kangaroo-sql-builder.png)
![数据导出界面](./images/kangaroo-export.png)
![数据导入界面](./images/kangaroo-import.png)
![工具配置界面](./images/kangaroo-setting.png)
![快捷键配置](./images/kangaroo-shortcut.png)
